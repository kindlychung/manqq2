qq = function(hub) {
    o = hub$originalData$mlogp[!is.na(hub$originalData$mlogp)]
    o = sort(o, decreasing=TRUE)
    e = -log10(ppoints(length(o)))
    hub$qqdat = data.frame(e, o)
    qqplot = ggplot(hub$qqdat, aes(e, o)) + geom_point(alpha=.4) + xlab("Expected -log P") +
        ylab("Observed -log P") + geom_abline(intercept=0, slope=1, alpha=.3)
    return(qqplot)
}

qq2 = function(p1, p2, labels = c("p1","p2")) {
    stopifnot(is.numeric(p1))
    stopifnot(is.numeric(p2))
    stopifnot(is.character(labels))
    stopifnot(length(labels) == 2)
    l1 = length(p1)
    l2 = length(p2)
    len = min(l1, l2)
    o1 = sort(-log10(p1), decreasing = TRUE)
    o2 = sort(-log10(p2), decreasing = TRUE)
    if(l1 < l2) {
        o2 = o2[1:len]
    } else if(l2 < l1) {
        o1 = o1[1:len]
    }
    e1 = -log10(ppoints(len))
    e = rep(e1, 2)
    o = c(o1, o2)
    label = rep(labels, each = len)
    dat = data.frame(e = e, o = o, label = label)
    qqplot = ggplot(dat, aes(e, o, color=label)) + geom_point(alpha=.4) + xlab("Expected -log P") +
        ylab("Observed -log P") + geom_abline(intercept=0, slope=1, alpha=.3)
    qqplot
}
