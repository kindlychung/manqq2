# Requirements

* git
* R packages:
    * ggplot2
    * Rcpp
    * RcppArmadillo
    * devtools

# Installation

    require(devtools)
    install_bitbucket("kindlychung/txtutils")
    install_bitbucket("kindlychung/manqq2")


# Changes

* ditch r5 for s3

# To do

